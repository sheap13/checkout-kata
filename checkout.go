package main

type PriceObject struct {
    singleUnitPrice int
}

type CO struct {
    total int
    priceMap map[string]PriceObject
}

func (co *CO) Scan(item string) {
    co.total += co.priceMap[item].singleUnitPrice
}

func main() {
}
