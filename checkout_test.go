package main

import (
    "testing"
    "fmt"
)

func TestGetTotalInitialisesAsZero(t *testing.T) {
    co := CO{}
    if (co.total != 0) {
        t.Error("Expected total to initialise as 0")
    } else {
        fmt.Println("Pass: Total initialises to 0")
    }
}

func TestScanOneItemReturnsSetPrice(t *testing.T) {
    co := CO{}
    costOfA := 50
    co.priceMap = map[string]PriceObject{
        "A": PriceObject{costOfA},
    }
    co.Scan("A")
    if (co.total != costOfA) {
        t.Error("Expected total to be cost of one A after scanning one A")
    } else {
        fmt.Println("Pass: Total after scanning one A is cost of one A")
    }
}

func TestScanTwoItemsReturnsCostOfTwoItems(t *testing.T) {
    co := CO{}
    costOfA := 50
    costOfTwoAs := costOfA * 2
    co.priceMap = map[string]PriceObject{
        "A": PriceObject{costOfA},
    }
    co.Scan("A")
    co.Scan("A")
    if (co.total != costOfTwoAs) {
        t.Errorf("Expected total to be cost of two As after scanning two As. Expected: %d, actual: %d\n", costOfTwoAs, co.total)
    } else {
        fmt.Println("Pass: Total after scanning two As is cost of two As")
    }
}
